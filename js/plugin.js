$('.fa-info-circle').tooltip();
$("document").ready(function(){
    $('.calandar .calandar-div table td').on('click', function (e) {
        $('td').not(this).children("span").removeClass('focus'); //Remove focus from other TDs
        $(this).children("span").addClass('focus');
    });
    $('.custom-file-input').on('change',function(){
        $(this).next('.custom-file-label').addClass("selected").html($(this).val());
      })
      
});
